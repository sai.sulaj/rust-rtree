use super::utils;
use criterion::{criterion_group, BenchmarkId, Criterion, Throughput};
use rust_rtree::rtree::Rtree;

pub fn n_nearest_query_benchmark(c: &mut Criterion) {
    let features = utils::generate_random_features(1_000_000);
    let rtree = Rtree::from_reader(&&features[..], 64);

    let mut group = c.benchmark_group("N nearest query");
    for count in &[1, 10, 100, 1000] {
        group.throughput(Throughput::Elements(*count as u64));
        group.bench_with_input(BenchmarkId::from_parameter(count), count, |b, count| {
            b.iter(|| rtree.n_nearest(*count, 0., 0., &|_| true))
        });
    }
    group.finish();
}

criterion_group!(n_nearest_query, n_nearest_query_benchmark);
