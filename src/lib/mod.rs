mod n_nearest;
pub mod points_reader;
mod range;
pub mod rtree;
mod sort;

#[cfg(test)]
mod tests {
    use super::rtree::Rtree;
    use geojson;

    fn gen_point_grid(size_len: usize) -> Vec<geojson::Feature> {
        (0..size_len)
            .flat_map(|y: usize| {
                (0..size_len).map(move |x: usize| geojson::Feature {
                    bbox: None,
                    id: None,
                    properties: None,
                    foreign_members: None,
                    geometry: Some(geojson::Geometry::new(geojson::Value::Point(vec![
                        x as f64, y as f64,
                    ]))),
                })
            })
            .collect()
    }

    fn get_point_grid_offset(pitch: usize, x: usize, y: usize) -> usize {
        y * pitch + x
    }

    #[test]
    fn test_index_and_range_1_result() {
        let pitch = 5;
        let features = gen_point_grid(pitch);
        let rtree = Rtree::from_reader(&&features[..], 2);
        let bbox = vec![0.5f64, 0.5f64, 1.5f64, 1.5f64];
        let result = rtree.range(&bbox);
        assert_eq!(result.len(), 1);
        assert_eq!(result[0], get_point_grid_offset(pitch, 1, 1));
    }

    #[test]
    fn test_index_and_range_2_results() {
        let pitch = 5;
        let features = gen_point_grid(pitch);
        let rtree = Rtree::from_reader(&&features[..], 2);
        let bbox = vec![0.5f64, 0.5f64, 2.5f64, 2.5f64];
        let result = rtree.range(&bbox);
        assert_eq!(result.len(), 4);
        assert!(result.contains(&get_point_grid_offset(pitch, 1, 1)));
        assert!(result.contains(&get_point_grid_offset(pitch, 1, 2)));
        assert!(result.contains(&get_point_grid_offset(pitch, 2, 1)));
        assert!(result.contains(&get_point_grid_offset(pitch, 2, 2)));
    }

    #[test]
    fn test_index_and_n_nearest_no_predicate() {
        let pitch = 5;
        let features = gen_point_grid(pitch);
        let rtree = Rtree::from_reader(&&features[..], 2);

        let result = rtree.n_nearest(9, 2., 2., &|_| true);
        assert_eq!(result.len(), 9);
        let result: Vec<usize> = result.iter().map(|v| v.0).collect();

        for x in 1..=3 {
            for y in 1..=3 {
                assert!(
                    result.contains(&get_point_grid_offset(pitch, x, y)),
                    "Did not contain: ({}, {}) = {}",
                    x,
                    y,
                    get_point_grid_offset(pitch, x, y)
                );
            }
        }
    }

    #[test]
    fn test_index_and_n_nearest_with_predicate_8_results() {
        let pitch = 5;
        let features = gen_point_grid(pitch);
        let rtree = Rtree::from_reader(&&features[..], 2);

        let result = rtree.n_nearest(8, 2., 2., &|index| {
            index != get_point_grid_offset(pitch, 2, 2)
        });
        assert_eq!(result.len(), 8);
        let result: Vec<usize> = result.iter().map(|v| v.0).collect();

        for x in 1..=3 {
            for y in 1..=3 {
                if x == 2 && y == 2 {
                    assert!(
                        !result.contains(&get_point_grid_offset(pitch, x, y)),
                        "Contains: ({}, {}) = {}",
                        x,
                        y,
                        get_point_grid_offset(pitch, x, y)
                    );
                } else {
                    assert!(
                        result.contains(&get_point_grid_offset(pitch, x, y)),
                        "Did not contain: ({}, {}) = {}",
                        x,
                        y,
                        get_point_grid_offset(pitch, x, y)
                    );
                }
            }
        }
    }

    #[test]
    fn test_index_and_n_nearest_with_predicate_1_result_filter_nearest() {
        let features: Vec<_> = (0..64 * 100)
            .map(|i| geojson::Feature {
                bbox: None,
                id: None,
                properties: None,
                foreign_members: None,
                geometry: Some(geojson::Geometry::new(geojson::Value::Point(vec![
                    i as f64, i as f64,
                ]))),
            })
            .collect();
        let rtree = Rtree::from_reader(&&features[..], 64);

        let result = rtree.n_nearest(1, 64. * 100., 64. * 100., &|index| index == 0);
        assert_eq!(result.len(), 1);
        assert_eq!(result[0].0, 0);
    }
}
