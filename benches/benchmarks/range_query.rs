use super::utils;
use criterion::{black_box, criterion_group, BatchSize, Criterion};
use rand::prelude::*;
use rust_rtree::rtree::Rtree;

// The random feature coords range between [-100, 100].
fn gen_bbox() -> Vec<f64> {
    let mut rng = thread_rng();

    vec![
        rng.gen_range(-100f64, 100f64),
        rng.gen_range(-100f64, 100f64),
        rng.gen_range(-100f64, 100f64),
        rng.gen_range(-100f64, 100f64),
    ]
}

pub fn range_query_benchmark(c: &mut Criterion) {
    let features = utils::generate_random_features(1_000_000);
    let rtree = Rtree::from_reader(black_box(&&features[..]), 64);

    c.bench_function("Execute range query on 1,000,000 points", |b| {
        b.iter_batched(
            || gen_bbox(),
            |bbox| rtree.range(black_box(&bbox)),
            BatchSize::SmallInput,
        )
    });
}

criterion_group!(range_query, range_query_benchmark);
