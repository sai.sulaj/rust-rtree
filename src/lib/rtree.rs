use crate::n_nearest::n_nearest;
use crate::points_reader::PointsReader;
use crate::range;
use crate::sort::sort;
use geojson::Bbox;

pub type CalcDistance = fn(f64, f64, f64, f64) -> f64;
pub type Predicate<'a> = &'a dyn Fn(usize) -> bool;

#[derive(PartialEq, Clone, Debug)]
pub enum Axis {
    X,
    Y,
}
impl Axis {
    pub fn other(&self) -> Self {
        match self {
            Self::X => Self::Y,
            Self::Y => Self::X,
        }
    }
}
#[derive(Debug, Clone)]
pub struct IndexPositionPair {
    pub index: usize,
    pub x: f64,
    pub y: f64,
}
impl IndexPositionPair {
    pub fn new(index: usize, x: f64, y: f64) -> Self {
        Self { index, x, y }
    }

    pub fn get_axis(&self, axis: &Axis) -> f64 {
        match axis {
            Axis::X => self.x,
            Axis::Y => self.y,
        }
    }
}

fn distance_euclidean(x1: f64, y1: f64, x2: f64, y2: f64) -> f64 {
    let mut dx = x2 - x1;
    dx *= dx;
    let mut dy = y2 - y1;
    dy *= dy;
    (dx + dy).sqrt()
}

pub struct Rtree {
    points: Vec<IndexPositionPair>,
    node_size: usize,
    bbox: Bbox,
    calc_distance: CalcDistance,
}
impl Rtree {
    pub fn new(
        points: Vec<IndexPositionPair>,
        bbox: Bbox,
        node_size: usize,
        calc_distance: CalcDistance,
    ) -> Self {
        Self {
            points,
            bbox,
            node_size,
            calc_distance,
        }
    }

    pub fn from_reader_cdist<R: PointsReader>(
        reader: &R,
        node_size: usize,
        calc_dist: CalcDistance,
    ) -> Self {
        let mut points: Vec<IndexPositionPair> = Vec::with_capacity(reader.size_hint());
        let mut min_x = f64::MAX;
        let mut min_y = f64::MAX;
        let mut max_x = -f64::MAX;
        let mut max_y = -f64::MAX;

        reader.visit_all(|index, x, y| {
            points.push(IndexPositionPair::new(index, x, y));
            if x < min_x {
                min_x = x;
            } else if x > max_x {
                max_x = x;
            }
            if y < min_y {
                min_y = y;
            } else if y > max_y {
                max_y = y;
            }
        });

        sort(&mut points, node_size, &Axis::X);

        Self::new(
            points,
            vec![min_x, min_y, max_x, max_y],
            node_size,
            calc_dist,
        )
    }

    pub fn from_reader<R: PointsReader>(reader: &R, node_size: usize) -> Self {
        Self::from_reader_cdist(reader, node_size, distance_euclidean)
    }

    pub fn range(&self, bbox: &Bbox) -> Vec<usize> {
        range::range(&self.points, self.node_size, bbox)
    }

    pub fn n_nearest(&self, n: usize, x: f64, y: f64, predicate: Predicate) -> Vec<(usize, f64)> {
        n_nearest(
            &self.points,
            self.bbox.clone(),
            self.node_size,
            n,
            x,
            y,
            predicate,
            self.calc_distance,
        )
    }
}
impl std::fmt::Debug for Rtree {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Rtree {{ points: {:?}, node_size: {:?}, bbox: {:?} }}",
            self.points, self.node_size, self.bbox
        )
    }
}
