use criterion::criterion_main;

mod benchmarks;

criterion_main! {
    benchmarks::building_index::building_index,
    benchmarks::range_query::range_query,
    benchmarks::n_nearest_query::n_nearest_query,
}
