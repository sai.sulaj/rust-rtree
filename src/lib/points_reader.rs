use geojson;

// I struggled to find a good way to accept any input
// that can be converted to an array of Points, using
// the std::convert::From trait for example. None worked, and so
// I found the idea for this from https://github.com/pka/rust-kdbush.
pub trait PointsReader {
    fn size_hint(&self) -> usize;
    fn visit_all<F>(&self, visitor: F)
    where
        F: FnMut(usize, f64, f64);
}

impl PointsReader for &[geojson::Feature] {
    fn size_hint(&self) -> usize {
        self.len()
    }

    fn visit_all<F>(&self, mut visitor: F)
    where
        F: FnMut(usize, f64, f64),
    {
        for (index, feature) in self.iter().enumerate() {
            if let Some(geometry) = feature.geometry.as_ref() {
                if let geojson::Value::Point(point) = &geometry.value {
                    visitor(index, point[0], point[1]);
                }
            }
        }
    }
}
