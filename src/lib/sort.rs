use crate::rtree::{Axis, IndexPositionPair};
use float_cmp::approx_eq;
use std::slice::from_raw_parts_mut;

// Same as split_at_mut except the mipoint is excluded from
// both slices.
fn split_at_exclusive<T>(arr: &mut [T], mid: usize) -> (&mut [T], &mut [T]) {
    let len = arr.len();
    assert!(mid <= len);

    // SAFETY: [ptr; mid) and (mid; len] are not overlapping,
    // so returning a mutable reference is fine.
    unsafe {
        let ptr = arr.as_mut_ptr();
        return (
            from_raw_parts_mut(ptr, mid),
            from_raw_parts_mut(ptr.add(mid + 1), len - mid - 1),
        );
    }
}

pub fn sort(points: &mut [IndexPositionPair], node_size: usize, axis: &Axis) {
    if points.len() <= node_size {
        return;
    }

    let m: usize = points.len() / 2;

    select(points, m, axis);

    let (left_arr, right_arr) = split_at_exclusive(points, m);

    rayon::join(
        || sort(left_arr, node_size, &axis.other()),
        || sort(right_arr, node_size, &axis.other()),
    );
}

// Taken from https://en.wikipedia.org/wiki/Floyd%E2%80%93Rivest_algorithm
// and https://github.com/mourner/kdbush
//
// Refactored to not use recursion due to stack overflows.
fn select(points: &mut [IndexPositionPair], k: usize, axis: &Axis) {
    let mut stack: Vec<(usize, usize, Axis)> = vec![(0, points.len() - 1, axis.clone())];

    while let Some((left, right, axis)) = stack.pop() {
        if right <= left {
            break;
        }

        // Use select recursively to sample a smaller set of size s.
        // The arbitrary constants 600 and 0.5 are used in the original
        // version to minimize execution time.
        if right - left > 600 {
            let n = (right - left + 1) as f64;
            let i = (k - left + 1) as f64;
            let z = n.ln();
            let s = 0.5f64 * (2f64 * z / 3f64).exp();
            let sd = 0.5 * (z * s * (n - s) / n).sqrt().copysign(i - n / 2f64);
            let new_left = left.min((k as f64 - i * s / n + sd) as usize);
            let new_right = right.min((k as f64 + (n - i) * s / n + sd) as usize);
            stack.push((new_left, new_right, axis.clone()));
        }

        // Partition the elements between left and right around t.
        let t = points[k].get_axis(&axis);
        let mut i = left;
        let mut j = right;

        points.swap(left, k);
        if points[right].get_axis(&axis) > t {
            points.swap(left, right);
        }

        while i < j {
            points.swap(i, j);
            i += 1;
            j -= 1;

            while points[i].get_axis(&axis) < t {
                i += 1;
            }
            while points[j].get_axis(&axis) > t {
                j -= 1;
            }
        }

        if approx_eq!(f64, points[left].get_axis(&axis), t) {
            points.swap(left, j);
        } else {
            j += 1;
            points.swap(j, right);
        }

        let mut next_left = left;
        let mut next_right = right;

        if j <= k {
            next_left = j + 1;
        }
        if k <= j {
            next_right = j - 1;
        }

        stack.push((next_left, next_right, axis));
    }
}
