use crate::rtree::{Axis, CalcDistance, IndexPositionPair, Predicate};
use float_cmp::approx_eq;
use geojson::Bbox;
use std::cmp::Ordering;
use std::collections::BinaryHeap;

fn is_leaf(node_size: usize, slice: &[IndexPositionPair]) -> bool {
    slice.len() <= node_size
}

struct Points<'a> {
    slice: &'a [IndexPositionPair],
    bbox: Bbox,
    axis: Axis,
}
impl<'a> Points<'a> {
    pub fn new(slice: &'a [IndexPositionPair], bbox: Bbox, axis: Axis) -> Self {
        Self { slice, bbox, axis }
    }

    pub fn left_child(&self) -> Self {
        let mid = self.slice.len() / 2;
        let mid_p = &self.slice[mid];
        let left_slice = &self.slice[..=mid - 1];

        let max_x = match self.axis {
            Axis::X => mid_p.x,
            Axis::Y => self.bbox[2],
        };
        let max_y = match self.axis {
            Axis::Y => mid_p.y,
            Axis::X => self.bbox[3],
        };

        Self::new(
            left_slice,
            vec![self.bbox[0], self.bbox[1], max_x, max_y],
            self.axis.other(),
        )
    }

    pub fn right_child(&self) -> Self {
        let mid = self.slice.len() / 2;
        let mid_p = &self.slice[mid];
        let right_slice = &self.slice[mid + 1..];

        let min_x = match self.axis {
            Axis::X => mid_p.x,
            Axis::Y => self.bbox[0],
        };
        let min_y = match self.axis {
            Axis::Y => mid_p.y,
            Axis::X => self.bbox[1],
        };

        Self::new(
            right_slice,
            vec![min_x, min_y, self.bbox[2], self.bbox[3]],
            self.axis.other(),
        )
    }

    pub fn split(&self) -> (Self, Self) {
        (self.left_child(), self.right_child())
    }

    // Returns the lower bound of the distance from a location to points inside the bbox.
    pub fn bbox_dist(&self, x: f64, y: f64, calc_dist: CalcDistance) -> f64 {
        let min_x = self.bbox[0];
        let min_y = self.bbox[1];
        let max_x = self.bbox[2];
        let max_y = self.bbox[3];

        let left = x < min_x;
        let right = x > max_x;
        let above = y > max_y;
        let below = y < min_y;

        // Inside.
        if !left && !right && !above && !below {
            return 0.;
        }

        // Point to corners.
        if left && above {
            return calc_dist(x, y, min_x, max_y);
        } else if left && below {
            return calc_dist(x, y, min_x, min_y);
        } else if right && above {
            return calc_dist(x, y, max_x, max_y);
        } else if right && below {
            return calc_dist(x, y, max_x, min_y);
        }

        // Point to sides.
        if !left && !right {
            if above {
                calc_dist(x, y, x, max_y)
            } else {
                calc_dist(x, y, x, min_y)
            }
        } else if left {
            calc_dist(min_x, y, x, y)
        } else {
            calc_dist(max_x, y, x, y)
        }
    }
}

enum HeapData<'a> {
    HeapDataPoints(Points<'a>),
    HeapDataPoint(&'a IndexPositionPair),
}

struct HeapItem<'a> {
    dist: f64,
    data: HeapData<'a>,
}
impl<'a> HeapItem<'a> {
    pub fn new(dist: f64, data: HeapData<'a>) -> Self {
        Self { dist, data }
    }

    pub fn is_point(&self) -> bool {
        if let HeapData::HeapDataPoint(_) = self.data {
            return true;
        }
        false
    }
}
impl<'a> PartialOrd for HeapItem<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
impl<'a> Ord for HeapItem<'a> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.dist
            .partial_cmp(&other.dist)
            .map(|o| o.reverse()) // Reverse for min heap.
            .unwrap_or(Ordering::Equal)
    }
}
impl<'a> PartialEq for HeapItem<'a> {
    fn eq(&self, other: &Self) -> bool {
        approx_eq!(f64, self.dist, other.dist)
    }
}
impl<'a> Eq for HeapItem<'a> {}

pub fn n_nearest(
    points: &[IndexPositionPair],
    bbox: Bbox,
    node_size: usize,
    n: usize,
    x: f64,
    y: f64,
    predicate: Predicate,
    calc_dist: CalcDistance,
) -> Vec<(usize, f64)> {
    let mut results: Vec<(usize, f64)> = Vec::new();
    let mut queue: BinaryHeap<HeapItem> = BinaryHeap::new();

    // Node representing all data.
    let mut curr_node = Some(HeapItem::new(
        0.,
        HeapData::HeapDataPoints(Points::new(points, bbox, Axis::X)),
    ));

    while let Some(node) = curr_node {
        if let HeapData::HeapDataPoints(points) = node.data {
            if is_leaf(node_size, points.slice) {
                // Add all points of the leaf node to the queue.
                let points_iter = points
                    .slice
                    .iter()
                    .filter(|point| predicate(point.index))
                    .map(|point| {
                        HeapItem::new(
                            calc_dist(x, y, point.x, point.y),
                            HeapData::HeapDataPoint(point),
                        )
                    });
                queue.extend(points_iter);
            } else {
                let mid = points.slice.len() / 2;
                let mid_p = &points.slice[mid];

                // Add midpoint to queue.
                if predicate(mid_p.index) {
                    queue.push(HeapItem::new(
                        calc_dist(x, y, mid_p.x, mid_p.y),
                        HeapData::HeapDataPoint(mid_p),
                    ));
                }

                let (left_points, right_points) = points.split();
                let left_dist = left_points.bbox_dist(x, y, calc_dist);
                let right_dist = right_points.bbox_dist(x, y, calc_dist);
                let left_node = HeapData::HeapDataPoints(left_points);
                let right_node = HeapData::HeapDataPoints(right_points);
                queue.push(HeapItem::new(left_dist, left_node));
                queue.push(HeapItem::new(right_dist, right_node));
            }
        }

        while queue.peek().map(|v| v.is_point()).unwrap_or(false) {
            let heap_item = queue.pop().unwrap();
            if let HeapData::HeapDataPoint(point) = heap_item.data {
                results.push((point.index, heap_item.dist));
                if results.len() == n {
                    return results;
                }
            }
        }

        curr_node = queue.pop();
    }

    results
}
