use rand::prelude::*;
use rust_rtree::rtree::Rtree;

fn main() {
    let mut rng = rand::thread_rng();
    let num = 10000000;

    let features: Vec<_> = (0..num)
        .map(|_| geojson::Feature {
            bbox: None,
            id: None,
            properties: None,
            foreign_members: None,
            geometry: Some(geojson::Geometry::new(geojson::Value::Point(vec![
                rng.gen_range(-100f64, 100f64),
                rng.gen_range(-100f64, 100f64),
            ]))),
        })
        .collect();

    let rtree = Rtree::from_reader(&&features[..], 64);
    for _ in 0..5 {
        let range = vec![
            rng.gen_range(-100f64, 100f64),
            rng.gen_range(-100f64, 100f64),
            rng.gen_range(-100f64, 100f64),
            rng.gen_range(-100f64, 100f64),
        ];
        let indices = rtree.range(&range);
        println!("Len: {}", indices.len()); // Want to ensure that the above call is not optimized out.
    }
}
