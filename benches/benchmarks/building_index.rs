use super::utils;
use criterion::{criterion_group, BenchmarkId, Criterion, Throughput};
use rust_rtree::rtree::Rtree;

pub fn building_index_benchmark(c: &mut Criterion) {
    let features = utils::generate_random_features(10_000_000);

    let mut group = c.benchmark_group("Index points");
    for count in &[10_000, 100_000, 1_000_000, 10_000_000] {
        let input_slice = &features[0..*count];
        group.throughput(Throughput::Elements(*count as u64));
        group.bench_with_input(
            BenchmarkId::from_parameter(count),
            input_slice,
            |b, input_slice| b.iter(|| Rtree::from_reader(&input_slice, 64)),
        );
    }
    group.finish();
}

criterion_group!(building_index, building_index_benchmark);
