use geojson;
use rand::prelude::*;

pub fn generate_random_features(num: usize) -> Vec<geojson::Feature> {
    let mut rng = rand::thread_rng();

    let features: Vec<_> = (0..num)
        .map(|_| geojson::Feature {
            bbox: None,
            id: None,
            properties: None,
            foreign_members: None,
            geometry: Some(geojson::Geometry::new(geojson::Value::Point(vec![
                rng.gen_range(-100f64, 100f64),
                rng.gen_range(-100f64, 100f64),
            ]))),
        })
        .collect();

    features
}
