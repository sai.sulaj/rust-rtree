use crate::rtree::{Axis, IndexPositionPair};
use geojson::Bbox;
use std::collections::VecDeque;

macro_rules! init_stack {
    ($($x:expr),*) => {{
        let mut s = VecDeque::new();
        $(s.push_front($x);)*
        s
    }};
}

struct StackItem<'a> {
    axis: Axis,
    slice: &'a [IndexPositionPair],
}
impl<'a> StackItem<'a> {
    pub fn new(axis: Axis, slice: &'a [IndexPositionPair]) -> Self {
        Self { axis, slice }
    }
}

pub fn range(points: &[IndexPositionPair], node_size: usize, bbox: &Bbox) -> Vec<usize> {
    let mut stack: VecDeque<StackItem> = init_stack!(StackItem::new(Axis::X, points));
    let mut result: Vec<usize> = Vec::new();

    while let Some(StackItem { axis, slice }) = stack.pop_front() {
        // If we reach a tree node, search linearly.
        if slice.len() <= node_size {
            let res_iter = slice
                .iter()
                .filter(|p| p.x >= bbox[0] && p.x <= bbox[2] && p.y >= bbox[1] && p.y <= bbox[3])
                .map(|p| p.index);
            result.extend(res_iter);
            continue;
        }

        // Otherwise, find the middle index.
        let m: usize = slice.len() / 2;
        let mp = &slice[m];

        // Include the middle item if it is in range.
        if mp.x >= bbox[0] && mp.x <= bbox[2] && mp.y >= bbox[1] && mp.y <= bbox[3] {
            result.push(mp.index);
        }

        // queue search in halves that intersect the query.
        match axis {
            Axis::X => {
                if bbox[0] <= mp.x {
                    let next_slice = &slice[..=m - 1];
                    stack.push_front(StackItem::new(axis.other(), next_slice));
                }
                if bbox[2] >= mp.x {
                    let next_slice = &slice[m + 1..];
                    stack.push_front(StackItem::new(axis.other(), next_slice));
                }
            }
            Axis::Y => {
                if bbox[1] <= mp.y {
                    let next_slice = &slice[..=m - 1];
                    stack.push_front(StackItem::new(axis.other(), next_slice));
                }
                if bbox[3] >= mp.y {
                    let next_slice = &slice[m + 1..];
                    stack.push_front(StackItem::new(axis.other(), next_slice));
                }
            }
        };
    }

    result
}
